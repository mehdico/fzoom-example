import 'package:flutter/material.dart';
import 'package:fzoom_example/screens/start_non_login_meeting.dart';

class PreStartNonloginMeetingWidget extends StatelessWidget {
  final tecUserId = TextEditingController();
  final tecMeetingId = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Start Meeting (non login)'),
        ),
        body: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(hintText: 'User Id (Email)'),
                  controller: tecUserId,
                ),
                TextField(
                  decoration: InputDecoration(hintText: 'Meeting Id'),
                  controller: tecMeetingId,
                ),
                Builder(
                  builder: (context) {
                    return RaisedButton(
                      onPressed: () => launchJoinMeeting(context),
                      child: Text('Start'),
                    );
                  },
                ),
              ],
            )));
  }

  launchJoinMeeting(BuildContext context) {
    String userId = tecUserId.text;
    String meetingId = tecMeetingId.text;

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return StartNonloginMeetingWidget(userId, meetingId);
        },
      ),
    );
  }
}
