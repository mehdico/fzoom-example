import 'package:flutter/material.dart';
import 'package:fzoom_example/screens/start_meeting.dart';

class PreStartMeetingWidget extends StatelessWidget {
  final tecUserId = TextEditingController();
  final tecPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Start Meeting'),
        ),
        body: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(hintText: 'User Id (Email)'),
                  controller: tecUserId,
                ),
                TextField(
                  decoration: InputDecoration(hintText: 'Password'),
                  controller: tecPassword,
                ),
                Builder(
                  builder: (context) {
                    return RaisedButton(
                      onPressed: () => launchJoinMeeting(context),
                      child: Text('Start'),
                    );
                  },
                ),
              ],
            )));
  }

  launchJoinMeeting(BuildContext context) {
    String userId = tecUserId.text;
    String password = tecPassword.text;

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return StartMeetingWidget(userId, password);
        },
      ),
    );
  }
}
