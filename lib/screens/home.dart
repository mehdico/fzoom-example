import 'package:flutter/material.dart';
import 'package:fzoom_example/screens/pre_join_meeting.dart';
import 'package:fzoom_example/screens/pre_start-non-login_meeting.dart';
import 'package:fzoom_example/screens/pre_start_meeting.dart';

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fzoom library Example'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 32.0,
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Builder(
                  builder: (context) {
                    return RaisedButton(
                      onPressed: () => joinMeeting(context),
                      child: Text('Join meeting'),
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Builder(
                  builder: (context) {
                    return RaisedButton(
                      onPressed: () => startMeetingWithLogin(context),
                      child: Text('Start meeting (login)'),
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Builder(
                  builder: (context) {
                    return RaisedButton(
                      onPressed: () => startMeetingNoLogin(context),
                      child: Text('Start meeting (Non-Login)'),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  joinMeeting(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return PreJoinMeetingWidget();
        },
      ),
    );
  }

  startMeetingWithLogin(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return PreStartMeetingWidget();
        },
      ),
    );
  }

  startMeetingNoLogin(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return PreStartNonloginMeetingWidget();
        },
      ),
    );
  }
}
