import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fzoom/zoom_options.dart';
import 'package:fzoom/zoom_view.dart';
import 'package:fzoom/zoom_result.dart';
import 'package:fzoom_example/constants.dart';

class JoinMeetingWidget extends StatelessWidget {
  ZoomJoinMeetingParams meetingParams;

  final String meetingId;
  final String meetingPassword;

  JoinMeetingWidget(this.meetingId, this.meetingPassword, {Key key}) : super(key: key) {
    this.meetingParams = new ZoomJoinMeetingParams(
        domain: Constants.DOMAIN,
        appKey: Constants.APP_KEY,
        appSecret: Constants.APP_SECRET,
        meetingId: meetingId,
        meetingPassword: meetingPassword,
        displayName: "YOUR NAME",
        disableDialIn: "true",
        disableDrive: "true",
        disableInvite: "true",
        disableShare: "true",
        noAudio: "false",
        noDisconnectAudio: "false");
  }

  bool _isMeetingEnded(String status) {
    var result = false;

    if (Platform.isAndroid)
      result = status == "MEETING_STATUS_DISCONNECTING" || status == "MEETING_STATUS_FAILED";
    else
      result = status == "MEETING_STATUS_IDLE";

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Loading meeting' + meetingId),
      ),
      body: Padding(
          padding: EdgeInsets.all(16.0),
          child: ZoomView(onViewCreated: (controller) {
            debugPrint("Created the view");

            controller.joinMeetingNoLogin(this.meetingParams).then((result) {
              if (result == ZoomResult.SUCCESS) {
                controller.zoomStatusEvents.listen((status) {
                  debugPrint("Meeting Status Stream: " + status[0] + " - " + status[1]);
                  if (_isMeetingEnded(status[0])) {
                    Navigator.pop(context);
                  }
                });
                debugPrint("listen on event channel");
              }
            }).catchError((error) {
              debugPrint("Error:" + error.toString());
            });
          })),
    );
  }
}
