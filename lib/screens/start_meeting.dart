import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fzoom/zoom_options.dart';
import 'package:fzoom/zoom_view.dart';
import 'package:fzoom/zoom_result.dart';
import 'package:fzoom_example/constants.dart';

class StartMeetingWidget extends StatelessWidget {
  ZoomStartMeetingParams meetingParams;

  final String userId;
  final String password;

  StartMeetingWidget(this.userId, this.password, {Key key}) : super(key: key) {
    this.meetingParams = new ZoomStartMeetingParams(
        appKey: Constants.APP_KEY,
        appSecret: Constants.APP_SECRET,
        domain: Constants.DOMAIN,
        userId: userId,
        password: password,
        disableDialIn: "true",
        disableDrive: "true",
        disableInvite: "true",
        disableShare: "true",
        noVideo: "false",
        noDisconnectAudio: "false");
  }

  bool _isMeetingEnded(String status) {
    var result = false;

    if (Platform.isAndroid)
      result = status == "MEETING_STATUS_DISCONNECTING" || status == "MEETING_STATUS_FAILED";
    else
      result = status == "MEETING_STATUS_IDLE";

    return result;
  }

  @override
  Widget build(BuildContext context) {
    ZoomView zoomView = new ZoomView(onViewCreated: (controller) {
      debugPrint("Created the view");

      controller.startMeeting(this.meetingParams).then((result) {
        if (result == ZoomResult.SUCCESS) {
          controller.zoomStatusEvents.listen((status) {
            debugPrint("Meeting Status Stream: " + status[0] + " - " + status[1]);
            if (_isMeetingEnded(status[0])) {
              Navigator.pop(context);
            } else if (status[0] == "MEETING_STATUS_INMEETING") {
              controller.getCurrentMeetingInviteInfo().then((inviteInfoResult) {
                if (inviteInfoResult[1] == ZoomResult.SUCCESS) {
                  debugPrint(
                      "meetingInfo:\nnumber: ${inviteInfoResult[1]}\npassword: ${inviteInfoResult[2]}\nurl: ${inviteInfoResult[3]}");
                } else {
                  //no meeting
                }
              });
            }
          });
          debugPrint("listen on event channel");
        } else if (result == ZoomResult.ERROR_LOGIN) {
          debugPrint('auth failed');
        }
      }).catchError((error) {
        debugPrint("Error:" + error.toString());
      });
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('Loading meeting'),
      ),
      body: Padding(padding: EdgeInsets.all(16.0), child: zoomView),
    );
  }
}
