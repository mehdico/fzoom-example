import 'package:flutter/material.dart';
import 'package:fzoom_example/screens/join_meeting.dart';

class PreJoinMeetingWidget extends StatelessWidget {
  final tecMeetingId = TextEditingController();
  final tecMeetingPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Join Meeting'),
        ),
        body: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(hintText: 'Meeting ID'),
                  controller: tecMeetingId,
                ),
                TextField(
                  decoration: InputDecoration(hintText: 'Meeting Password'),
                  controller: tecMeetingPassword,
                ),
                Builder(
                  builder: (context) {
                    return RaisedButton(
                      onPressed: () => launchJoinMeeting(context),
                      child: Text('Join'),
                    );
                  },
                ),
              ],
            )));
  }

  launchJoinMeeting(BuildContext context) {
    String meetingId = tecMeetingId.text;
    String meetingPassword = tecMeetingPassword.text;

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return JoinMeetingWidget(meetingId, meetingPassword);
        },
      ),
    );
  }
}
