import 'package:flutter/material.dart';
import 'package:fzoom_example/screens/home.dart';
import 'package:fzoom_example/screens/pre_join_meeting.dart';
import 'package:fzoom_example/screens/pre_start-non-login_meeting.dart';
import 'package:fzoom_example/screens/pre_start_meeting.dart';

void main() => runApp(ExampleApp());

class ExampleApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fzoom library Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      navigatorObservers: [],
      initialRoute: '/',
      routes: {
        '/': (context) => HomeWidget(),
        '/PreJoinMeeting': (context) => PreJoinMeetingWidget(),
        '/PreStartMeeting': (context) => PreStartMeetingWidget(),
        '/PreStartNonloginMeeting': (context) => PreStartNonloginMeetingWidget(),
      },
    );
  }
}
